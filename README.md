# XlsxWriter
* https://github.com/jmcnamara/XlsxWriter // 2.4k stars // last commit - 10 days ago
* https://xlsxwriter.readthedocs.io/
* write +
* read -
* format  xlsx

# openpyxl
* https://github.com/ericgazoni/openpyxl // 115 stars // last commit - 7 years ago
* https://foss.heptapod.net/openpyxl/openpyxl // 17 stars // last commit - 3 days ago
* https://openpyxl.readthedocs.io/en/stable/
* write +
* read +
* format xlsx

# pylightxl
* https://github.com/PydPiper/pylightxl // 129 stars // last commit - 6 days ago
* https://pylightxl.readthedocs.io/en/latest/quickstart.html
* write +
* read +
* format xlsx csv xlsm

-------------------------------------------------------------------------------------------------

# xlwings
* https://github.com/xlwings/xlwings // 1.9k stars // last commit - 2 months ago
* https://docs.xlwings.org/en/stable/
* windows/mac
* read -
* write +

# xlrd
* https://github.com/python-excel/xlrd // 1.8k stars // last commit - last month
* https://xlrd.readthedocs.io/en/latest/
* write -
* read +
* format xls
* python 3.5

# xlwt
* https://github.com/python-excel/xlwt // 900 stars // last commit - 2 years ago
* https://xlwt.readthedocs.io/en/stable/
* write +
* read - 
* format xls
* python 3.5

=================================================================================================

# Writing performance

```
Versions:
python: 3.6.9
openpyxl: 3.0.1
xlsxwriter: 1.2.5

Dimensions:
    Rows = 1000
    Cols = 50
    Sheets = 1
    Proportion text = 0.10

Times:
    xlsxwriter            :   0.59
    xlsxwriter (optimised):   0.54
    openpyxl              :   0.73
    openpyxl (optimised)  :   0.61


Versions:
python: 3.7.5
openpyxl: 3.0.1
xlsxwriter: 1.2.5

Dimensions:
    Rows = 1000
    Cols = 50
    Sheets = 1
    Proportion text = 0.10

Times:
    xlsxwriter            :   0.65
    xlsxwriter (optimised):   0.53
    openpyxl              :   0.70
    openpyxl (optimised)  :   0.63


Versions:
python: 3.8.0
openpyxl: 3.0.1
xlsxwriter: 1.2.5

Dimensions:
    Rows = 1000
    Cols = 50
    Sheets = 1
    Proportion text = 0.10

Times:
    xlsxwriter            :   0.54
    xlsxwriter (optimised):   0.50
    openpyxl              :   1.10
    openpyxl (optimised)  :   0.57
```
