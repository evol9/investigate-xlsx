from openpyxl import Workbook
from tempfile import NamedTemporaryFile

wb = Workbook()

ws = wb.active

ws.title = "My title"

ws.sheet_properties.tabColor = "1072BA"

ws['A1'] = 'Hello'
ws['A2'] = 'World'


wb.save('openpyxl.xlsx')



wb = Workbook()
ws = wb.active

ws.title = "My title"

ws['A1'] = 'Hello'
ws['A2'] = 'World'

with NamedTemporaryFile() as tmp:
	wb.save(tmp.name)
	tmp.seek(0)
	stream = tmp.read()
