from contextlib import closing
from io import BytesIO

import xlsxwriter

workbook = xlsxwriter.Workbook('xlsxwriter.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'Hello world')
worksheet.write('A2', 'boom')

workbook.close()


# server

def writex():
	with closing(BytesIO()) as output:
		with closing(xlsxwriter.Workbook(output, {'in_memory': True})) as workbook:
			worksheet = workbook.add_worksheet()
			worksheet.write(0, 0, 'Hello, world!')

		output.seek(0)
		return output.read()

print(writex())
